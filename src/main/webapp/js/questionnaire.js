var questionTemplate = new Object();
var numOfQuestions;
//var domain = "http://5.101.107.163:8080/Questionnaire/";
var domain = "http://localhost:8080/Questionnaire/";

// Disable caching for AJAX calls
$.ajaxSetup({cache: false});

$(document).ready(function () {
    initPanels();
    initCallbacks();
    initQuestionnaire();
    
});

/*
// for the window resize
$(window).resize(function() {
    var bodyheight = $(document).height();
    $("#sidebar").width(bodyheight * 0.8);
});
*/

function initPanels() {
    $("#fill-panel").show();
    $("#fill-panel").css('font-size', '20px');      
    $("#end-panel").hide();
    $("#footer").hide();
}

function initCallbacks() {
    $("#q-button").click(openQuestions);
    $("#restart-button").click(reset);
    $("#send-results-btn").click(sendResults);
    $("#back-results-btn").click(reset);
    $("#log-in").submit(requestResults);
    $( "#popupLogin" ).popup({
        afteropen: function( event, ui ) {
            disableScroll();
        },
        afterclose: function ( event, ui ) {
            //alert('closing');
            enableScroll();
        },
        popupafterclose: function ( event, ui ) {
            enableScroll();
        }
    });
}

function reset() {
    $('#questionnaire-panel input').each(function (i, e) {
        $input = $(e);
        $input.attr("checked", false).checkboxradio('refresh');
        //alert($input.attr("checked"));
    });
    $("#fill-panel").hide();
    $("#end-panel").hide();
    $("#start-panel").show();
    $("#dev-text").show();
    $("#license-text").show();
    $("#result-btn").show();
    $("#footer").hide();
    // Scroll back up
    $.mobile.silentScroll(0);
}

function initQuestionnaire() {
    var done = false;
    var Q_PREFIX = 'Σενάριο ';
    var A_PREFIX = 'Μεταβείτε σε απόσταση ';
    var A_MID = 'm έναντι αμοιβής ';
    var A_SUFIX = '€';
    var url = domain + 'QuestionCountServlet';
    var tempArr = new Array();
    $.get(url, function (data) {
        numOfQuestions = data;
        alert(numOfQuestions);
        $q = $("#questionnaire-panel");
        counter = 1;
        for (var j = 1; j <= numOfQuestions; j += 1) {
            var url = domain + 'questions/' + j + '.que';
            $.get(url, function (data) {
                var qJson = jQuery.parseJSON(data);
                //console.log('Q : ' + j + ' ' + qJson.answers + ' ' + qJson.question);
                $fieldset = $('<fieldset data-role="controlgroup" data-mini="false">');
                $fieldset.data("aid", qJson.id);
                $legend = $("<legend style=\"font-weight: bold;\">");
                $legend.html(Q_PREFIX+' '+qJson.id);
                $fieldset.append($legend);
 
                $.each(qJson.answers, function (i, e) {
                    $input = $('<input type="radio" name="radio-choice' + counter + '" id="radio-choice-' + counter + '_' + i + '" value="' + i + '">');
                    $label = $('<label for="radio-choice-' + counter + '_' + i + '">');

                    var tokens = e.split('-');
                    $label.html( A_PREFIX + tokens[1] + A_MID + tokens[0] + A_SUFIX );
                    
                    $fieldset.append($input);
                    $fieldset.append($label);
                });

                counter++;

                tempArr[tempArr.length] = $fieldset.get(0);
                //$q.append($fieldset).enhanceWithin();
            });
        }
    
        $(document).ajaxStop(function () {
            tempArr.sort(function (a, b) {
                var contentA = parseInt( $(a).data("aid") );
                var contentB = parseInt( $(b).data("aid") );
                return (contentA - contentB);
            });
            for (index = 0; index < tempArr.length; index++) {
                //console.log($(tempArr[index]).data("aid"));
                $q.append($(tempArr[index])).enhanceWithin();
            }
        });

    
    });
}

function sendResults() {
    
    var d = new Date();
    var start = $("#fill-panel").data("start_time");
    var end = d.getTime();
    var duration = Math.ceil( ( end - start ) / 1000 );
    var valid = true;
    var answers = new Array();
    //alert("what");
    $("#questionnaire-panel fieldset").each( function ( i, e ) {
        $fieldset = $(e);
        var answer = new Object();
        answer.val = $fieldset.find("input[type='radio']:checked").val();
        answer.id = $fieldset.data("aid");
        if(typeof answer.val === 'undefined') {
            alert("Παρακαλώ συμπληρώστε όλο το ερωτηματολόγιο");
            
            valid = false;
            
            return false;
        }
        answers[answers.length] = answer;
        
    });
    
    if ( !valid ) 
        return;
    
    var url = domain + "FillServlet";
   $.ajax({
        // request type
        type: "POST",
        // the URL for the request
        url: url,
        // the data to send (will be converted to a query string)
        dataType: "text",
        // code to run if the request succeeds;
        data: {"answers": JSON.stringify(answers), "duration": duration}, 
        // the response is passed to the function
        success: function (response) {
            //alert(response);
            $("#fill-panel").hide();
            $("#end-panel").show();
            $("#result-btn").show();
            $("#start-panel").hide();
            $("#footer").hide();
        },
        // code to run if the request fails; the raw request and
        // status codes are passed to the function
        error: function (xhr, status, errorThrown) {
            alert("Sorry, there was a problem!");
            alert("Error: " + errorThrown);
            alert("Status: " + status);
            alert(xhr);
        },
        // code to run regardless of success or failure
        complete: function (xhr, status) {
            //$('#connLabel').text("connected");
        }
    });
}

function openQuestions() {
    $.mobile.silentScroll(0);
    
    $("#start-panel").hide();
    $("#license-text").hide();
    $("#dev-text").hide();
    $("#end-panel").hide();
    $("#result-btn").hide();
    $("#fill-panel").show();
    $("#footer").show();
    
    var d = new Date();
    $("#fill-panel").data("start_time", d.getTime());
}

function requestResults( event ) {
    var values = $('#log-in').serialize();
    $.ajax({
        // request type
        type: "POST",
        // the URL for the request
        url: domain + "LoginServlet",
        // the data to send (will be converted to a query string)
        data: values,
        // the type of data we expect back
        dataType: "text",
        // code to run if the request succeeds;
        // the response is passed to the function
        success: function (response) {
            if ( response === 'done' ) {
                downloadResults();
            } else {
                alert("Λάθος κωδικός!");
            }
            //alert(response);
        },
        // code to run if the request fails; the raw request and
        // status codes are passed to the function
        error: function (xhr, status, errorThrown) {
            alert("Sorry, there was a problem!");
            console.log("Error: " + errorThrown);
            console.log("Status: " + status);
            console.dir(xhr);
        },
        // code to run regardless of success or failure
        complete: function (xhr, status) {
            //$('#connLabel').text("connected");
        }
    });
    event.preventDefault();
}

function downloadURL(url, callback){
   var d = new Date();
   var count = d.getTime();
   var hiddenIFrameID = 'hiddenDownloader' + count;
   var iframe = document.createElement('iframe');
   iframe.id = hiddenIFrameID;
   iframe.style.display = 'none';
   document.body.appendChild(iframe);
   iframe.src = url;
   callback();
}

function downloadResults( event ) {
   var url = domain + "ResultsServlet";
   window.location.href = url;
   $( "#popupLogin" ).popup( "close" );
   //$( "#popupClose" ).click();
   /*
   downloadURL(url, function() {; $( "#popupLogin" ).popup( "close" ); });
   */
}

var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;  
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
  if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel = null; 
    window.ontouchmove = null;  
    document.onkeydown = null;  
}
