/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.msc.cs.mds.questionnaire;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.channels.FileLock;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nick
 */
@WebServlet(name = "FillServlet", urlPatterns = {"/FillServlet"})
public class FillServlet extends HttpServlet {

    private static final boolean CLEAR_DB = false;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.INDENT_OUTPUT, false);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

            long time = System.currentTimeMillis();

            //response.setContentType(fileType);
            response.setHeader("Access-Control-Allow-Origin", "*");
            //response.setHeader("Cache-control", "no-cache, no-store");
            //response.setHeader("Pragma", "no-cache");
            //response.setHeader("Expires", "-1");
            response.setHeader("Date", Long.toString(time));
            response.setHeader("Last-Modified", Long.toString(time));
            //response.setHeader("Content-disposition","attachment; filename=results.csv");
            //response.setContentLength(sb.length());            

            String answers = request.getParameter("answers");

            if ( answers == null ) {
                out.print("Empty answers");
                
                return;
            }
            
            String home = System.getProperty("user.home");
            System.out.println(home);
            JsonFactory factory = mapper.getJsonFactory(); // since 2.1 use mapper.getFactory() instead
            JsonParser jp = factory.createJsonParser(answers);
            JSONAnswer[] ans = mapper.readValue(jp, JSONAnswer[].class);
            
            HttpSession sess = request.getSession();
            if ( sess == null ) {
                out.print("Invalid session");
                
                return;
            }
            Integer qCount = (Integer) sess.getAttribute("qCount");
            if ( null == qCount) {
                out.print("Invalid session");
                
                return;
            }
            
            if ( ans.length != qCount ) {
                out.print("Invalid session");
                
                return;
            }
            
            //System.out.println("Answers size " + ans.length);
            
            List<Submission> resultList = null;
            File resultsDir = new File(home + "/QuestionnaireData");
            File backupDir = new File(home + "/QuestionnaireData/backup");
            resultsDir.mkdirs();
            backupDir.mkdirs();
            File resultsFile = new File(home + "/QuestionnaireData/results.db");
            if (!resultsFile.exists()) {
                resultsFile.createNewFile();
                resultList = new ArrayList<>();
            } else {
                FileInputStream fis = new FileInputStream(resultsFile);
                try {
                    FileLock lock = fis.getChannel().tryLock(0L, Long.MAX_VALUE, true);
                    while (lock == null) {
                        lock = fis.getChannel().tryLock(0L, Long.MAX_VALUE, true);
                    }
                    try {
                        ObjectInputStream ois = new ObjectInputStream(fis);
                        resultList = (List<Submission>) ois.readObject();
                    } finally {
                        lock.release();
                    }
                } finally {
                    fis.close();
                }
            }

            //System.out.println("Results size "+resultList.size());
            
            if (CLEAR_DB) {
                resultList.clear();
            }
            
            int duration = Integer.parseInt( request.getParameter("duration") );
            Date date=new Date(time);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+3"));
            Submission sub = new Submission(getClientIpAddr(request), Integer.toString(resultList.size()), sdf.format(date), ans, duration);
            resultList.add(sub);

            File resultsBackupFile = new File(home + "/QuestionnaireData/backup/results_"+resultList.size()+".db");
            FileOutputStream fosb = new FileOutputStream(resultsBackupFile, false);
            try {
                FileLock lock = fosb.getChannel().tryLock();
                while (lock == null) {
                    lock = fosb.getChannel().tryLock();
                }
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(fosb);
                    oos.writeObject(resultList);
                } finally {
                    lock.release();
                }
            } finally {
                fosb.close();
            }
            
            FileOutputStream fos = new FileOutputStream(resultsFile, false);
            try {
                FileLock lock = fos.getChannel().tryLock();
                while (lock == null) {
                    lock = fos.getChannel().tryLock();
                }
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(resultList);
                } finally {
                    lock.release();
                }
            } finally {
                fos.close();
            }
        }
    }
    
    private static String getClientIpAddr(HttpServletRequest request) {  
        String ip = request.getHeader("X-Forwarded-For");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        }  
        return ip;  
    }  

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FillServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FillServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
