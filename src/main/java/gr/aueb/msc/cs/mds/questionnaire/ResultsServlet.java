/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.msc.cs.mds.questionnaire;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nickvitsas
 */
@WebServlet(name = "ResultsServlet", urlPatterns = {"/ResultsServlet"})
public class ResultsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/csv;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            HttpSession sess = request.getSession();
            if ( sess == null )
                return;
            
            Boolean logged = (Boolean)sess.getAttribute("logged");
            if ( logged == null ) {
                response.setHeader("Access-Control-Allow-Origin", "*");
                out.print("error");
                out.flush();
                out.close();
                
                return;
            }
            
            if ( !logged ) {
                response.setHeader("Access-Control-Allow-Origin", "*");
                out.print("error");
                out.flush();
                out.close();
                
                return;
            }
            
            String fileType = "application/octet-stream";
            StringBuilder sb = new StringBuilder();

            long time = System.currentTimeMillis() + TimeZone.getDefault().getRawOffset();

            response.setContentType(fileType);
            response.setHeader("Access-Control-Allow-Origin", "*");
            //response.setHeader("Cache-control", "no-cache, no-store");
            //response.setHeader("Pragma", "no-cache");
            //response.setHeader("Expires", "-1");
            //response.setHeader("Date", Long.toString(time));
            //response.setHeader("Last-Modified", Long.toString(time));
            response.setHeader("Content-disposition", "attachment; filename=results.csv");

            String home = System.getProperty("user.home");

            List<Submission> resultList = null;
            File resultsFile = new File(home + "/QuestionnaireData/results.db");
            if (!resultsFile.exists()) {
                sb.append("No entries!");
                response.setContentLength((int) sb.length());
                out.println(sb);
            } else {
                FileInputStream fis = new FileInputStream(resultsFile);
                try {
                    FileLock lock = fis.getChannel().tryLock(0L, Long.MAX_VALUE, true);
                    while (lock == null) {
                        lock = fis.getChannel().tryLock(0L, Long.MAX_VALUE, true);
                    }
                    try {
                        ObjectInputStream ois = new ObjectInputStream(fis);
                        resultList = (List<Submission>) ois.readObject();
                    } finally {
                        lock.release();
                    }
                } finally {
                    fis.close();
                }
                
                Submission first = resultList.get(0);
                out.print("KEY,");
                for ( int i = 0; i < first.getAns().length; i++ ) {
                    out.print("Q"+(i+1)+",");
                }
                out.print("DATE,");
                out.print("DURATION,");
                out.println("IP");
                for (Submission sub : resultList) {
                    out.print(sub.getId()+",");
                    for (JSONAnswer an : sub.getAns()) {
                        out.print( an.getVal()+ ",");
                    }
                    out.print(sub.getDate()+",");
                    out.print(sub.getDuration()+",");
                    out.println(sub.getIp());
                }
            }
            
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ResultsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ResultsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
