/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.msc.cs.mds.questionnaire;

import java.io.Serializable;

/**
 *
 * @author nickvitsas
 */
public class Submission implements Serializable {
    String id;
    String date;
    String ip;
    int duration;
    JSONAnswer[] ans;

    public Submission(String ip, String id, String date, JSONAnswer[] ans, int duration) {
        this.id = id;
        this.date = date;
        this.ans = ans;
        this.duration = duration;
        this.ip = ip;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public JSONAnswer[] getAns() {
        return ans;
    }

    public void setAns(JSONAnswer[] ans) {
        this.ans = ans;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
    
}
