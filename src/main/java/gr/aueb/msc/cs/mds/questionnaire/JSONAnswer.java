/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.msc.cs.mds.questionnaire;

import java.io.Serializable;

/**
 *
 * @author nickvitsas
 */
public class JSONAnswer implements Serializable {

    private int val;
    private int id;

    public JSONAnswer() {
    }

    public JSONAnswer(int val, int id) {
        this.val = val;
        this.id = id;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}